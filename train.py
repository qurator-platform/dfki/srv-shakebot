from chatterbot import ChatBot
from chatterbot.trainers import ListTrainer
import pandas as pd

chatbot = ChatBot("Frank")

corpus = pd.read_csv('corpora/hamlet.csv', sep='\t')
trainer = ListTrainer(chatbot)
trainer.train(corpus["original"])