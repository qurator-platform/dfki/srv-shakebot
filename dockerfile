from python:3

COPY serve.py .
COPY db.sqlite3 .
COPY static static/
COPY corpora corpora/
COPY templates templates/

RUN pip install chatterbot
RUN pip install chatterbot-corpus
RUN pip install flask

EXPOSE 8080
ENTRYPOINT FLASK_APP=serve.py flask run --host=0.0.0.0 --port=8080
